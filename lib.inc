section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax,
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax,rax
	.loop:
		cmp byte [rdi+rax], 0	; Сравниваем код символа с нулем
		je .end					; Если 0, строка закончилась => выход
		inc	rax					; нет - увеличиваем счетчик длины
		jmp .loop
	.end:		
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    call string_length
    mov rdx, rax	; количество байт для записи
    mov rsi, rdi	; адрес строки
    mov rax, 1		; номер системного вызова write
    mov rdi, 1		; дескриптор stdout
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi		; используем стек
    xor rax, rax
    mov rsi, rsp	; адрес строки
    mov rax, 1		; номер системного вызова write
    mov rdi, 1		; дескриптор stdout
    mov rdx, 1		; количество байт для записи
    syscall
    pop	rdi
    ret



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	xor rax, rax
	xor rcx, rcx
	mov rax, rdi
	mov rdi, 10				; помещаем 10 (делитель) в rdi
	mov r9, rsp				; начальное состояние rsp
	push 0					; обозначаем конец в стеке
	.loop:
		xor rdx, rdx
		div rdi				; делим на 10
		add rdx, 0x30		; перевод в ascii
		dec rsp
		mov byte[rsp], dl	; записываем цифру в стек
		test rax, rax		; логическое "и"
		jnz .loop			; если не 0
	mov rdi, rsp			; выводим rsp
	call print_string		; выводим rsp
	mov rsp, r9				; возвращаем rsp в исходное
	ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi,rdi	; логическое "и"
	jns	.print		; если > 0
	push rdi		; используем стек
	mov	rdi,'-'		; знак
	call print_char	; выводим "-"
	pop	rdi			; удаляем код знака из регистра
	neg	rdi			; делаем > 0, чтобы вывести
	.print:
		call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rax, rax
	push rbx
	call string_length
	mov rcx, rax				; записываем в rcx длину строки
	.loop:
		mov dl, byte[rdi+rcx]	; В младший байт rdx записываем один байт первой строки
		mov bl, byte[rsi+rcx]	; В младший байт rbx записываем один байт второй строки
		cmp dl, bl				; Сравниваем младшие байты этих двух регистров
		jne .not_equal			; Если не равны - строки не равны
		cmp bl, 0				; Если равны, проверяем, не закончилась ли строка
		je .equal				; Если закончилась, выводим 1
		dec rcx					; уменьшаем счетчик
		jnz .loop
	.equal:
		pop rbx
		mov rax, 1
		ret
	.not_equal:
		pop rbx
		mov rax, 0
		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor	rax,rax
	push 0
	mov	rdx,1		; количество байт для чтения
	mov	rsi,rsp		; адрес строки (стек)
	mov	rdi,0		; дескриптор stdin
	syscall
	pop	rax
	ret

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rcx, rcx
	.loop:
		cmp rcx, rsi			; если размера буфера не хватает (rcx>rsi)
		jg .error				; выходим с ошибкой
		push rdx				; сохраняем состояние
		push rcx				; сохраняем состояние
		push rdi				; сохраняем состояние
		push rsi				; сохраняем состояние
		mov rdi, rdx	
		call read_char			; считываем символ
		pop rsi					; возвращаем в исходное состояние
		pop rdi					; возвращаем в исходное состояние
		pop rcx					; возвращаем в исходное состояние
		pop rdx			
		cmp al, 0x20			; сравниваем младший байт rax c кодом пробела
		je .continue
		cmp al, 0xA				; сравниваем младший байт rax c кодом перевода строки
		je .continue
		cmp al, 0x9				; сравниваем младший байт rax c кодом табуляции
		je .continue
		cmp al, 0				; сравниваем младший байт rax c кодом конца строки, если да, то выходим
		je .end			
		mov byte[rdi+rcx], al	; записываем в буфер наш символ
		inc rcx					; нарасчиваем счетчик
		jmp .loop
	.error:
		mov rax, 0				
		ret
	.continue:
		test rcx, rcx			; если это начало строки, то пропускаем этот символ, если не начало, то выходим
		jz .loop
	.end:
		mov byte[rdi+rcx], al
		mov rax, rdi
		mov rdx, rcx
		ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor	rax,rax
	xor	rdx,rdx
    push rbx
	.loop:
		xor	rbx,rbx
		mov	bl,byte[rdi+rdx]	; записываем в младший байт rbx символ строки
		cmp	bl, '0'				; проверяем конец строки
		jb	.end				; выходим, если строка кончилась
		cmp	bl,'9'				; проверяем 
		ja	.end
		sub	bl,'0'              
		imul rax,10				; умножаем на 10
		add	rax,rbx				; в rbx хранится последняя цифра, таким образом мы преобразуем числов строку 234 = (2*10+3)*10+4
		inc	rdx					; счетчик длины строки
		jnz	.loop
	.end:
		pop	rbx
		ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rdx, rdx
	xor rax, rax
	xor rcx, rcx
	push rdi
	.sign:
		cmp byte[rdi+rcx], '-'	; смотрим, есть ли знак
		je .neg
		call parse_uint
		jmp .exit
	.neg:
		inc rdi
		call parse_uint
		neg rax
		inc rdx
	.exit:
		pop rdi
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера (rdi, rsi, rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
	.loop:	
		mov al, byte[rdi]	; в младший байт rax записываем байт rdi
		mov byte[rsi], al	; в младший байт rsi записываем байт rdi
		inc rdi				; увеличиваем указатель на строку
		inc rsi				; увеличиваем указатель на буфер
		cmp al, 0			; проверяем конец строки
		jne .loop
		ret

